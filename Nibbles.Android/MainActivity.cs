﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using CocosSharp;
using Microsoft.Xna.Framework;
using wambrella.Shared;

namespace wambrella.Android
{
	[Activity (
		Label = "wambrella",
		AlwaysRetainTaskState = true,
		Icon = "@drawable/ic_launcher",
		Theme = "@android:style/Theme.NoTitleBar",
		ScreenOrientation = ScreenOrientation.Portrait | ScreenOrientation.ReversePortrait,
		LaunchMode = LaunchMode.SingleInstance,
		MainLauncher = true,
		ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden)]
	public class MainActivity : AndroidGameActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			var application = new CCApplication ();
			application.ApplicationDelegate = new GameAppDelegate ();
			SetContentView (application.AndroidContentView);
			application.StartGame ();
		}
	}
}


