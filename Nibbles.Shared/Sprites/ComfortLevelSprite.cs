using Box2D.Dynamics;

namespace wambrella.Shared.Sprites
{
    public class ComfortLevelSprite : BasePhysicalSprite
    {
        public ComfortLevelSprite(b2World world)
            : base(world)
        {
        }
    }
}