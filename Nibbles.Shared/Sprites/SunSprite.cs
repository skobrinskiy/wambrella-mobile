﻿using CocosSharp;

namespace wambrella.Shared.Sprites
{
    public class SunSprite: SpriteWithColorGradientAndAlphaChannel
    {
        public SunSprite(CCColor4B startColor, CCColor4B endColor, float textureWidth, float textureHeight) : base(startColor, endColor, textureWidth, textureHeight)
        {
        }

        public SunSprite(CCColor4B startColor, CCColor4B endColor, CCSize textureSizeInPixels) : base(startColor, endColor, textureSizeInPixels)
        {
        }

        protected override string AlphaChannelFileName
        {
            get { return "sun"; }
        }

        protected override void GenerateGradient(CCColor4B color)
        {
        }

        protected override void InitVertices()
        {
        }
    }
}
