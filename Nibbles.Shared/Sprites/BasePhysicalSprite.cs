using Box2D.Collision.Shapes;
using Box2D.Common;
using Box2D.Dynamics;
using CocosSharp;
using System.Collections.Generic;
using wambrella.Shared.Helpers;

namespace wambrella.Shared.Sprites
{
    public abstract class BasePhysicalSprite : CCSprite
    {
        private b2Body _body;
        private readonly b2World _world;
        private b2BodyDef _ballBodyDef;
        private b2CircleShape _circle;
        private b2Fixture _fixture;
        private float _density;
        private bool _hasHugeDensity;

        private CCLabel _label;

        protected virtual float SpriteScale
        {
            get { return 1.0f; }
        }

        public float Radius
        {
            get { return SpriteScale*ContentSize.Height/2.0f; }
        }

        private string _text;

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                UpdateText();
            }
        }

        public SortedSet<BasePhysicalSprite> CollidesWith { get; private set; } = new SortedSet<BasePhysicalSprite>();

        public bool IsEffectFinalized { get; set; }

        public string ActivityName { get; set; }

        public int Index { get; set; }

        private CCPoint PreviousPosition { get; set; }

        public float StartPositionX { get; set; }

        public float StartPositionY { get; set; }

        public float PopUpPositionX { get; set; }

        public float PopUpPositionY { get; set; }

        public bool IsTouched { get; set; }

        protected BasePhysicalSprite(b2World world)
            : base("bubble")
        {
            _world = world;
        }

        protected virtual void CreateBallBodyAndShape()
        {
            _ballBodyDef = new b2BodyDef
            {
                type = b2BodyType.b2_dynamicBody,
                active = true,
                allowSleep = false,
                userData = this,
            };
            _ballBodyDef.position.Set(StartPositionX/Constants.PtmRatio, StartPositionY/Constants.PtmRatio);

            _body = _world.CreateBody(_ballBodyDef);
            _body.GravityScale = 0.0f;

            _circle = new b2CircleShape
            {
                Radius = SpriteScale*ContentSize.Width/(2.0f*Constants.PtmRatio),
                ShapeType = b2ShapeType.e_circle
            };

            var ballShapeDef = new b2FixtureDef
            {
                shape = _circle,
                density = 1.0f,
                friction = 1.0f,
                restitution = 0.0f,
//                                filter = {categoryBits = Constants.CategoryBubble, maskBits = Constants.MaskBubble, groupIndex = 1}
            };

            _fixture = _body.CreateFixture(ballShapeDef);

            Position = new CCPoint(StartPositionX, StartPositionY);
            AnchorPoint = CCPoint.AnchorMiddle;
            Scale = SpriteScale;
        }

        private void SetDensity(float value)
        {
            _fixture.Density = value;
            _body.ResetMassData();
        }

        public void MakeHugeDensity()
        {
            if (_hasHugeDensity) return;

            _density = _fixture.Density;
            _fixture.Density = Constants.HugeValue;
            _body.ResetMassData();

            _hasHugeDensity = true;
        }

        public void RestoreDensity()
        {
            if (!_hasHugeDensity) return;

            _fixture.Density = _density;
            _body.ResetMassData();

            _hasHugeDensity = false;
        }

        protected virtual void CreateLabel()
        {
            if (_label != null)
            {
                return;
            }

            _label = new CCLabel(Text, Constants.BubbleFontName, Constants.ComfortLevelBallTextFontSize)
            {
                Position = ContentSize.Center,
                AnchorPoint = CCPoint.AnchorMiddle,
                Color = CCColor3B.White,
            };

            AddChild(_label);
        }

        protected virtual void UpdateText()
        {
            if (_label == null)
            {
                return;
            }

            _label.Text = _text;
        }

        public virtual void UpdatePosition()
        {
            Position = new CCPoint(_body.Position.x*Constants.PtmRatio, _body.Position.y*Constants.PtmRatio);
            //            Rotation = -1 * CCMacros.CCRadiansToDegrees(_body.Angle);
        }

        protected virtual void ElasticPopUp(float dt)
        {
            var move = new CCMoveTo(1, new CCPoint(PopUpPositionX, PopUpPositionY));
            var accel = new CCEaseElasticOut(move, Constants.ElasticPeriod) {Duration = Constants.PopUpSpeed};
            var callback = new CCCallFunc(FinalizePopUp);
            RunAction(new CCSequence(accel, callback));
        }

        public virtual void ChangePosition(float x, float y)
        {
            Position = new CCPoint(x, y);
        }

        public void Teleport()
        {
            _body.SetTransform(new b2Vec2(PositionX/Constants.PtmRatio, PositionY/Constants.PtmRatio), _body.Angle);
        }

        public virtual void SimpleReturnToPopUpPosition()
        {
            SimpleMove(PopUpPositionX, PopUpPositionY);
        }

        protected virtual void SimpleMove(float x, float y)
        {
            var move = new CCMoveTo(1, new CCPoint(x, y));
            var callback = new CCCallFunc(FinalizeSimpleMove);
            RunAction(new CCSequence(move, callback));
        }

        public virtual void ElasticReturnToPopUpPosition()
        {
            ElasticMove(PopUpPositionX, PopUpPositionY);
        }

        protected virtual void ElasticMove(float x, float y)
        {
            var move = new CCMoveTo(1, new CCPoint(x, y));
            var accel = new CCEaseElasticOut(move, Constants.ElasticPeriod) {Duration = Constants.MoveSpeed};
            var callback = new CCCallFunc(FinalizeElasticMove);
            RunAction(new CCSequence(accel, callback));
        }

        protected virtual void FinalizeSimpleMove()
        {
            IsEffectFinalized = true;
        }

        protected virtual void FinalizeElasticMove()
        {
            IsEffectFinalized = true;
        }

        protected virtual void FinalizePopUp()
        {
            IsEffectFinalized = true;
        }

        protected virtual void PhysicalSyncBodyPosition()
        {
            if (_body == null) return;

            var fromCurrentToNewPosition = new b2Vec2(Position.X/Constants.PtmRatio - _body.Position.x,
                Position.Y/Constants.PtmRatio - _body.Position.y);
            var distanceToNewPosition = fromCurrentToNewPosition.Normalize();
            var velocityRequired = distanceToNewPosition/Constants.Timestep*fromCurrentToNewPosition;
            _body.LinearVelocity = velocityRequired;
        }

        public override CCPoint Position
        {
            get { return base.Position; }

            set
            {
                PreviousPosition = base.Position;
                base.Position = value;

                PhysicalSyncBodyPosition();
            }
        }

        protected virtual void InitEventListeners()
        {
            var aListener = new CCEventListenerAccelerometer
            {
                IsEnabled = true,
                OnAccelerate = OnAccelerate
            };
            AddEventListener(aListener, this);
        }

        protected virtual void OnAccelerate(CCEventAccelerate ccEventAccelerate)
        {
            var gravity = new b2Vec2((float) ccEventAccelerate.Acceleration.Y*30.0f,
                -(float) ccEventAccelerate.Acceleration.X*30.0f);
            _world.Gravity = gravity;
        }

        protected virtual void PopUp()
        {
            ScheduleOnce(ElasticPopUp, 1);
        }

        protected override void AddedToScene()
        {
            base.AddedToScene();

            CreateBallBodyAndShape();

            CreateLabel();

            InitEventListeners();

            PopUp();
        }

        public virtual void StopBody()
        {
            _body.AngularDamping = Constants.HugeValue;
            _body.LinearVelocity = new b2Vec2(0.0f, 0.0f);
            _body.AngularVelocity = 0;
        }
    }
}