using CocosSharp;

namespace wambrella.Shared.Sprites
{
    public class SpriteWithColorGradientAndAlphaChannel : CCSprite
    {
        private CCColor4B _startColor;
        private CCColor4B _endColor;
        private readonly CCSize _textureSizeInPixels;

        protected readonly CCRenderTexture RenderTexture;
        protected readonly CCDrawNode GradientNode;
        private readonly CCV3F_C4B[] _vertices = new CCV3F_C4B[6];
        protected readonly CCSprite Alpha;

        protected virtual string AlphaChannelFileName
        {
            get { return "alphachannel"; }
        }

        public CCColor4B StartColor
        {
            get { return _startColor; }
            set
            {
                if (_startColor == value) return;

                _startColor = value;
                MakeColorFull();
            }
        }

        public CCColor4B EndColor
        {
            get { return _endColor; }
            set
            {
                if (_endColor == value) return;

                _endColor = value;
                MakeColorFull();
            }
        }

        private void MakeColorFull()
        {
            // 2: Call CCRenderTexture:begin
            RenderTexture.BeginWithClear(_startColor);

            // 3: Draw into the texture
            GenerateGradient(_endColor);

            Alpha.Visit();

            // 4: Call CCRenderTexture:end
            RenderTexture.End();

            Texture = RenderTexture.Texture;
        }

        public SpriteWithColorGradientAndAlphaChannel(CCColor4B startColor, CCColor4B endColor, float textureWidth,
            float textureHeight)
            : this(startColor, endColor, new CCSize(textureWidth, textureHeight))

        {

        }

        public SpriteWithColorGradientAndAlphaChannel(CCColor4B startColor, CCColor4B endColor,
            CCSize textureSizeInPixels) : base()
        {
            _startColor = startColor;
            _endColor = endColor;
            _textureSizeInPixels = textureSizeInPixels;

            // 1: Create new CCRenderTexture
            RenderTexture = new CCRenderTexture(_textureSizeInPixels, _textureSizeInPixels);

            GradientNode = new CCDrawNode
            {
                AnchorPoint = new CCPoint(new CCVector2(0f, 0f)),
                Position = CCPoint.Zero,
                ContentSize = textureSizeInPixels
            };

            Alpha = new CCSprite(AlphaChannelFileName)
            {
                ContentSize = _textureSizeInPixels,
                AnchorPoint = new CCPoint(new CCVector2(0f, 0f)),
                Position = CCPoint.Zero,
                BlendFunc = new CCBlendFunc(CCOGLES.GL_DST_COLOR, CCOGLES.GL_ZERO),
            };

            InitVertices();

            MakeColorFull();
        }

        protected virtual void InitVertices()
        {
            // Left triangle TL - 0
            _vertices[0].Vertices = new CCVertex3F(0, _textureSizeInPixels.Height, 0);

            // Left triangle BL - 2
            _vertices[1].Vertices = new CCVertex3F(0, 0, 0);

            // Left triangle TR - 1
            _vertices[2].Vertices = new CCVertex3F(_textureSizeInPixels.Width, _textureSizeInPixels.Height, 0);

            // Right triangle BL - 2
            _vertices[3].Vertices = new CCVertex3F(0, 0, 0);

            // Right triangle BR - 3
            _vertices[4].Vertices = new CCVertex3F(_textureSizeInPixels.Width, 0, 0);

            // Right triangle TR - 1
            _vertices[5].Vertices = new CCVertex3F(_textureSizeInPixels.Width, _textureSizeInPixels.Height, 0);
        }

        /*
         *  TL    TR
         *   0----1 0,1,2,3 = index offsets for vertex indices
         *   |   /| 
         *   |  / |
         *   | /  |
         *   |/   |
         *   2----3
         *  BL    BR
         */

        protected virtual void GenerateGradient(CCColor4B color)
        {
            GradientNode.Clear();

            // Left triangle TL - 0
            _vertices[0].Colors = CCColor4B.Transparent;

            // Left triangle BL - 2
            _vertices[1].Colors = color;

            // Left triangle TR - 1
            _vertices[2].Colors = CCColor4B.Transparent;

            // Right triangle BL - 2
            _vertices[3].Colors = color;

            // Right triangle BR - 3
            _vertices[4].Colors = color;

            // Right triangle TR - 1
            _vertices[5].Colors = CCColor4B.Transparent;

            GradientNode.DrawTriangleList(_vertices);

            GradientNode.Visit();
        }
    }
}