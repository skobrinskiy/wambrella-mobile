namespace wambrella.Shared.Helpers
{
    public class Constants
    {
        public const float PtmRatio = 32.0f;

        public const float Timestep = 1.0f/300.0f;
        public const int VelocityIterations = 10;
        public const int PositionIterations = 10;

        public const float PopUpSpeed = 1.0f;
        public const float MoveSpeed = 3.0f;

#if __IOS__
        public const string BubbleFontName = "arial"; //Roboto-Light doesn't work with iOS
#else
        public const string BubbleFontName = "Roboto-Light";
#endif

        public const int ComfortLevelBallTextFontSize = 75; //for WP value will be taken from compiled font itself

        public const float ScreenVisibleAreaPercentage = .6f;

        public const float HorizontalDistanceBetweenPopUpedBubbles = 30.0f;

        public const float ElasticPeriod = 30.0f;

        public const float GravityX = 0.0f;
        public const float GravityY = 9.8f;

        public const float SunSize = 512.0f;

        public const int PopUpBubblesNumber = 3;

        //public const ushort CategoryBubble = 0x0001;
        //public const ushort CategoryScenery = 0x0002;

        //public const ushort MaskBubble = CategoryBubble | CategoryScenery;
        //public const ushort MaskScenery = 0x0003;

        public const float HugeValue = 9999999999999999999;

        public const int DisplaySkyHoursOnScreen = 8;
    }
}