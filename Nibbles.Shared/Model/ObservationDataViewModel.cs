﻿using System;
using System.Collections.Generic;

namespace wambrella.Shared.Model
{
    public class ObservationDataViewModel
    {
        public DateTime DateTime { get; set; }
        public IEnumerable<ActivityComfortLevel> Activities { get; set; }
    }
}
