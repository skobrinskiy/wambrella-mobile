﻿namespace wambrella.Shared.Model
{
    public class Parameter
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public string Metric { get; set; }
    }
}