﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Geolocation;

namespace wambrella.Shared.Model
{
    public delegate Task<IEnumerable<ObservationDataViewModel>> ObservationDataFunc(double latitude, double longitude);

    public sealed class Global
    {
        private static Global _instance;
        private static readonly object SyncRoot = new object();

        private Global()
        {
        }

        public static Global Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new Global();
                        }
                    }
                }

                return _instance;
            }
        }

        public async Task<IEnumerable<ObservationDataViewModel>> GetObservationData(ObservationDataFunc continuationFunction)
        {
#if __ANDROID__
            var geo = new Geolocator(global::Android.App.Application.Context);
#else
            var geo = new Geolocator();
#endif
            geo.DesiredAccuracy = 50;

            var task =
                await
                    geo.GetPositionAsync(10000)
                        .ContinueWith(t => continuationFunction(t.Result.Latitude, t.Result.Longitude));

            return await task;
        }
    }
}