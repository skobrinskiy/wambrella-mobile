﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wambrella.Shared.WambrellaPlatform;

namespace wambrella.Shared.Model
{
    public class Controller
    {
        public async Task<IEnumerable<ObservationDataViewModel>> GetActivityComfortLevelsForCurrentLocationPastAndFuture(double latitude,
            double longitude)
        {
            var observations = await WambrellaClient.GetActivityComfortLevelsForCurrentLocationPastAndFutureByHours(latitude, longitude);
            var groupedByDate = from act in observations.ActivityComfortLevels
                group act by act.DateTime.Date
                into g
                select new ObservationDataViewModel {DateTime = g.Key, Activities = g};

            return groupedByDate;
        }
    }
}
