﻿using System;
using System.Collections.Generic;

namespace wambrella.Shared.Model
{
    /// <summary>
    /// Observation data for specified evenet provider and/or location
    /// </summary>
    public class ObservationData
    {
        /// <summary>
        /// Name of event provider if it is existing in database; null otherwise
        /// </summary>
        public string EventProviderName { get; set; }

        /// <summary>
        /// ID of event provider if it is existing in the database; null otherwise
        /// </summary>
        public long? EventProviderId { get; set; }

        /// <summary>
        /// Name of location observations requested for
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// ID of location if it is existing in the database; null otherwise
        /// </summary>
        public long? LocationId { get; set; }

        /// <summary>
        /// Date and time of request in UTC-0
        /// </summary>
        public DateTime RequestDateTime { get; set; }

        /// <summary>
        /// Date and time of request in tome zone of location observations are being requested for
        /// </summary>
        public string StrRequestDateTime { get; set; }

        /// <summary>
        /// Collection of comfort level values for specified activity at specified date
        /// </summary>
        public IList<ActivityComfortLevel> ActivityComfortLevels { get; set; }

        /// <summary>
        /// Collection of values of weather parameters
        /// </summary>
        public IList<Parameter> Parameters { get; set; }
    }

    /// <summary>
    /// Value of comfort level for specified activity at specified date and time
    /// </summary>
    public class ActivityComfortLevel
    {
        /// <summary>
        /// Name of activity
        /// </summary>
        public string ActivityName { get; set; }

        /// <summary>
        /// Value of comfort level
        /// </summary>
        public decimal ComfortLevel { get; set; }

        /// <summary>
        /// Date comfort level is being calculated for 
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Date and time offset in <see cref="M:WeatherProvidersBase.DataRepository.WeatherRepository.DateTimeFormat"/> format
        /// </summary>
        public string StrDateTime { get; set; }
    }
}