using CocosSharp;
using CocosDenshion;
using wambrella.Shared.Layers;

namespace wambrella.Shared
{
    public class GameAppDelegate : CCApplicationDelegate
    {
        public override void ApplicationDidFinishLaunching(CCApplication application, CCWindow mainWindow)
        {
#if DEBUG
            mainWindow.DisplayStats = true;
            mainWindow.StatsScale = 3;
#endif

            application.PreferMultiSampling = false;
            application.ContentRootDirectory = "Content";
            application.ContentSearchPaths.Add("images");
            application.ContentSearchPaths.Add("fonts");

            var windowSize = mainWindow.WindowSizeInPixels;

            const float desiredWidth = 1024.0f;
            const float desiredHeight = 768.0f;

            CCScene.SetDefaultDesignResolution(desiredWidth, desiredHeight, CCSceneResolutionPolicy.FixedHeight);
            CCSprite.DefaultTexelToContentSizeRatio = desiredWidth < windowSize.Width ? 2.0f : 1.5f;

            var scene = MainLayer.CreateScene(mainWindow);
            mainWindow.RunWithScene(scene);
        }

        public override void ApplicationDidEnterBackground(CCApplication application)
        {
            application.Paused = true; // if you use SimpleAudioEngine, your music must be paused
            CCSimpleAudioEngine.SharedEngine.PauseBackgroundMusic();
        }

        public override void ApplicationWillEnterForeground(CCApplication application)
        {
            application.Paused = false; // if you use SimpleAudioEngine, your music must be paused
            CCSimpleAudioEngine.SharedEngine.ResumeBackgroundMusic();
        }
    }
}