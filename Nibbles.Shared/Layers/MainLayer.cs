using System;
using System.Collections.Generic;
using System.Linq;
using Box2D.Common;
using Box2D.Dynamics;
using CocosSharp;
using wambrella.Shared.Helpers;
using wambrella.Shared.Model;
using wambrella.Shared.Sprites;
using wambrella.Shared.Controllers;

namespace wambrella.Shared.Layers
{
    public class MainLayer : CCLayer
    {
        private b2World _world;
        private float _timeAccumulator;

        private CCBox2dDraw _debugDraw;

        private BubblesManager _manager;

        private BasePhysicalSprite _touchedBubble;

        int _opacityDirection = 1;

        private SunSprite _sun;

        private CCLayerColor _fog;

        private CCLayer _sky;
        private bool _skyScrolled;
        private short _skyScrollDirection;
        private CCPoint _previousSkyPosition = CCPoint.Zero;

        private IEnumerable<ObservationDataViewModel> _observationsData;

        private readonly Controller _controller = new Controller();

        public static CCScene CreateScene(CCWindow mainWindow)
        {
            var scene = new CCScene(mainWindow);
            var layer = new MainLayer();
            scene.AddChild(layer);

            return scene;
        }

        private void CheckCollision()
        {
            if (_manager == null) return;

            foreach (var bubble in _manager.Bubbles.Where(b => b.IsEffectFinalized))
            {
                if (bubble == null) continue;

                var rest = _manager.Bubbles.Where(b => b.Index != bubble.Index && b.IsEffectFinalized);

                foreach (var next in from next in rest
                                     let posX = bubble.Position.X - next.Position.X
                                     let posY = bubble.Position.Y - next.Position.Y
                                     let posXy = (posX * posX) + (posY * posY)
                                     let collision = (bubble.Radius + next.Radius) * (bubble.Radius + next.Radius)
                                     where posXy <= collision
                                     select next)
                {
                    //Collision

                    if (bubble.IsTouched)
                    {
                        next.StopBody();
                        bubble.CollidesWith.Add(next);
                    }
                    else
                    {
                        if (next.IsTouched)
                        {
                            bubble.StopBody();
                            next.CollidesWith.Add(bubble);
                        }
                    }
                }
            }
        }

        private void StartScheduling()
        {
            Schedule(t =>
            {
                if (t <= Constants.Timestep)
                    _timeAccumulator += t;
                else
                    _timeAccumulator += Constants.Timestep;

                CheckCollision();

                _world.ClearForces();

                while (_timeAccumulator >= Constants.Timestep)
                {
                    _timeAccumulator -= Constants.Timestep;

                    _world.Step(Constants.Timestep, Constants.VelocityIterations, Constants.PositionIterations);

                    for (var b = _world.BodyList; b != null; b = b.Next)
                    {
                        if (b.UserData == null) continue;

                        var bubble = (BasePhysicalSprite)b.UserData;
                        bubble.UpdatePosition();
                    }
                }
            });
        }

        private void InitWorld()
        {
            var gravity = new b2Vec2(Constants.GravityX, -Constants.GravityY);
            // Vector of acceleration ( the mark "-" is for down direction, because the y axis is upwards )
            _world = new b2World(gravity); // Create world with vector of acceleration

            _world.SetAllowSleeping(false);
            _world.SetContinuousPhysics(true);

            _manager = new BubblesManager(_world, VisibleBoundsWorldspace);

            StartScheduling();
        }

        private void InitBubbles()
        {
            var bubbles = _manager.InitComfortLevelBubbles(_observationsData, ContentSize.Width).OrderByDescending(b => b.PopUpPositionY);

            foreach (var bubble in bubbles)
            {
                AddChild(bubble);
            }
        }

        private void InitPhysics()
        {
            InitWorld();

#if DEBUG && !__ANDROID__
            EnableDebugMode();
#endif
            InitBubbles();

            InitEventsListeners();
        }

        private void InitSun()
        {
            _sun = new SunSprite(CCColor4B.Yellow, CCColor4B.Transparent, Constants.SunSize, Constants.SunSize)
            {
                AnchorPoint = new CCPoint(new CCVector2(0f, 0f)),
                Position = new CCPoint(0, ContentSize.Height - Constants.SunSize / 2.0f),
                Scale = 1.5f
            };
            AddChild(_sun);
        }

        private void InitFog()
        {
            _fog = new CCLayerColor(CCColor4B.White)
            {
                Opacity = 150
            };
            AddChild(_fog);
        }

        private void InitSky2()
        {
            //            var spriteBatch = new CCSpriteBatchNode("animations/sprites");
            CCSpriteFrameCache.SharedSpriteFrameCache.AddSpriteFrames("animations/sprites.plist");

            var winSize = Window.WindowSizeInPixels;

            _sky = new CCLayer
            {
                AnchorPoint = new CCPoint(new CCVector2(0f, 0f)),
                Position = CCPoint.Zero,
                ContentSize = winSize
            };

            var spriteWidth = winSize.Width / Constants.DisplaySkyHoursOnScreen;

            var sprites = new CCSprite[24];

            for (var i = 0; i < 24; i++)
            {
                var fileCounter = i + 1;
                var fileName = string.Format(fileCounter < 10 ? "w.circa.0{0}.png" : "w.circa.{0}.png", fileCounter);

                sprites[i] = new CCSprite(fileName)
                {
                    ContentSize = new CCSize(spriteWidth, winSize.Height),
                    Position = new CCPoint(i * spriteWidth, 0.0f),
                    AnchorPoint = new CCPoint(new CCVector2(0.0f, 0.0f)),
                };

                _sky.AddChild(sprites[i]);
            }

            var eventListener = new CCEventListenerTouchAllAtOnce
            {
                OnTouchesMoved = OnScrollSky,
                OnTouchesEnded = OnScrollSkyEnded,
                OnTouchesBegan = OnScrollSkyBegan
            };

            _sky.AddEventListener(eventListener);

            AddChild(_sky);

#if __ANDROID__
            _sky.ScaleY = _sky.ContentSize.Height/winSize.Height;
#endif
        }

        private void OnScrollSkyBegan(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            _skyScrolled = true;
        }

        private void OnScrollSkyEnded(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            if (!_skyScrolled)
            {
                return;
            }

            const int delta = 50;

            var action = new CCMoveBy(.5f, new CCPoint(_skyScrollDirection * delta, 0));

            _sky.RunAction(action);

            _skyScrolled = false;
        }

        private void OnScrollSky(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            if (_touchedBubble != null)
            {
                _skyScrolled = false;
                return;
            }

            var currentPosition = ccTouches[0].Location;

            const int delta = 10;
            var positionX = _sky.Position.X;

            if (currentPosition.X < _previousSkyPosition.X)
            {
                _skyScrollDirection = -1;
                positionX -= delta;
            }
            else
            {
                _skyScrollDirection = 1;
                positionX += delta;
            }

            _sky.PositionX = positionX;

            _previousSkyPosition = currentPosition;
        }

        private void InitParticle()
        {
            var ps = new CCParticleSystemQuad("animations/weather.plist")
            {
                Position = new CCPoint(ContentSize.Width / 2.0f, ContentSize.Height / 2.0f),
            };
            AddChild(ps);

#if WINDOWS_PHONE
            var renderTexture = CCTextureCache.SharedTextureCache.AddImage("particle_texture");
            ps.Texture = renderTexture;
#endif
            var ps2 = new CCParticleSystemQuad("animations/weather2.plist")
            {
                Position = new CCPoint(ContentSize.Width / 2.0f, 0),
            };
            AddChild(ps2);

#if WINDOWS_PHONE
            var renderTexture2 = CCTextureCache.SharedTextureCache.AddImage("particle_texture2");
            ps2.Texture = renderTexture2;
#endif
        }

        public override async void OnEnter()
        {
            base.OnEnter();

            InitSky2();

            InitSun();

            InitParticle();

            //_observationsData =
            //    await
            //        Global.Instance.GetObservationData(
            //            _controller.GetActivityComfortLevelsForCurrentLocationPastAndFuture);

            InitPhysics();

            InitFog();
        }

        private void UpdateBubblesChangePositions()
        {
            _manager.UpdateComfortLevelBubbles(_observationsData, DateTime.UtcNow);

            var actions = new CCFiniteTimeAction[_manager.Bubbles.Count];
            for (var i = 0; i < _manager.Bubbles.Count; i++)
            {
                actions[i] = new CCCallFunc(_manager.Bubbles[i].ElasticReturnToPopUpPosition);
            }
            RunActions(actions);
        }

        private void InitEventsListeners()
        {
            var tListener = new CCEventListenerTouchAllAtOnce
            {
                IsEnabled = true,
                OnTouchesBegan = OnTouchesBegan,
                OnTouchesMoved = OnTouchesMoved,
                OnTouchesEnded = OnTouchesEnded,
                OnTouchesCancelled = OnTouchesCancelled
            };
            AddEventListener(tListener, this);
        }

        private void OnTouchesCancelled(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            _manager.ReleaseBubble(ref _touchedBubble);
        }

        private void OnTouchesEnded(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            _manager.ReleaseBubble(ref _touchedBubble);
        }

        private void OnTouchesBegan(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            _touchedBubble = _manager.GetBubbleContainingPoint(ccTouches[0].Location);

            if (_touchedBubble == null) return;

            _skyScrolled = false;

            _touchedBubble.StopAllActions();
            _touchedBubble.IsTouched = true;
            _touchedBubble.MakeHugeDensity();
        }

        static byte RandomColorValue()
        {
            var random = CCRandom.GetRandomInt(0, 255);
            return (byte)random;
        }

        static CCColor3B RandomBrightColor()
        {
            while (true)
            {
                const float requiredBrightness = 192;
                var randomColor = new CCColor3B(new CCColor4B(
                    RandomColorValue(),
                    RandomColorValue(),
                    RandomColorValue(),
                    255));
                if (randomColor.R > requiredBrightness ||
                    randomColor.G > requiredBrightness ||
                    randomColor.B > requiredBrightness)
                {
                    return randomColor;
                }
            }

        }

        private void UpdateFogOpacity()
        {
            if (_fog.Opacity == 0)
            {
                _opacityDirection = 1;
            }
            else
            {
                if (_fog.Opacity == 192)
                {
                    _opacityDirection = -1;
                }
            }

            _fog.Opacity = (byte)(_fog.Opacity + _opacityDirection);
        }

        private void OnTouchesMoved(List<CCTouch> ccTouches, CCEvent ccEvent)
        {
            if (_touchedBubble != null)
            {
                _manager.MoveBubble(_touchedBubble, ccTouches[0].Location);
            }
            else
            {
                UpdateBubblesChangePositions();
                UpdateFogOpacity();
                _sun.Color = RandomBrightColor();
            }
        }

        private void EnableDebugMode()
        {
            _debugDraw = new CCBox2dDraw("Roboto-Light", (int)Constants.PtmRatio);
            _world.SetDebugDraw(_debugDraw);
            _debugDraw.AppendFlags(b2DrawFlags.e_shapeBit);
        }

        private void DrawDebugInfo()
        {
            if (_debugDraw != null)
            {
                _debugDraw.Begin();
                _world.DrawDebugData();
                _debugDraw.End();
            }
        }

        protected override void Draw()
        {
            base.Draw();
            DrawDebugInfo();
        }
    }
}