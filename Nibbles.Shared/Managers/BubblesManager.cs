using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Box2D.Dynamics;
using CocosSharp;
using wambrella.Shared.Helpers;
using wambrella.Shared.Model;
using wambrella.Shared.Sprites;

namespace wambrella.Shared.Controllers
{
    public class BubblesManager
    {
        private readonly b2World _world;
        private CCRect _visibleBounds;

        private readonly IList<BasePhysicalSprite> _bubbles = new List<BasePhysicalSprite>();

        public IList<BasePhysicalSprite> Bubbles
        {
            get { return _bubbles; }
        }

        public BubblesManager(b2World world, CCRect visibleBounds)
        {
            _world = world;
            _visibleBounds = visibleBounds;
        }

        private float CalculateBubblePopUpYPosition(float val, float radius)
        {
            var visibleArea = Constants.ScreenVisibleAreaPercentage - radius / (100.0f * Constants.PtmRatio);

            var area = _visibleBounds.Size.Height * visibleArea;
            var verticalPadding = _visibleBounds.Size.Height * ((1.0f - visibleArea) / 2.0f);

            var pos = val * area + verticalPadding;

            return pos;
        }

        public float CalculateBubblePopUpXPosition(int bubbleIndex)
        {
            var k = 1;
            if (bubbleIndex % 2 == 0)
            {
                k = -1;
            }

            return _visibleBounds.Size.Width / 2.0f + Constants.HorizontalDistanceBetweenPopUpedBubbles * bubbleIndex * k;
        }

        Random rnd = new Random();

        public IEnumerable<BasePhysicalSprite> InitComfortLevelBubbles(IEnumerable<ObservationDataViewModel> data, float width)
        {
            ClearBubbles();

            //var utc = DateTime.UtcNow;
            //var nowData = data.SingleOrDefault(d => d.DateTime.Date == utc.Date);

            //var groupedByActivityName = (from nowActivity in nowData.Activities
            //                             where Math.Abs((nowActivity.DateTime - utc).TotalMinutes) <= 60
            //                             select nowActivity).ToArray();

            //var activitiesNumber = Math.Min(Constants.PopUpBubblesNumber, groupedByActivityName.Count());

            var positionX = width / 2.0f;

            for (var i = 0; i < /*activitiesNumber*/3; i++)
            {
                var bubble = new ComfortLevelSprite(_world);

                //var activity = groupedByActivityName.ElementAt(i);
                var activity = new ActivityComfortLevel {ComfortLevel = /*(decimal) rnd.NextDouble()*/.66M};

                bubble.StartPositionX = positionX;
                bubble.StartPositionY = -bubble.Radius;
                bubble.PopUpPositionX = CalculateBubblePopUpXPosition(i);
                bubble.PopUpPositionY = CalculateBubblePopUpYPosition((float)activity.ComfortLevel, bubble.Radius);
                bubble.Text = ((int)(100 * activity.ComfortLevel)).ToString(CultureInfo.InvariantCulture);

                bubble.ActivityName = activity.ActivityName;
                bubble.Index = i;

                _bubbles.Add(bubble);
            }

            return _bubbles;
        }

        public void UpdateComfortLevelBubbles(IEnumerable<ObservationDataViewModel> data, DateTime dateTime)
        {
            foreach (var bubble in _bubbles)
            {
                var cl = rnd.NextDouble();
                bubble.PopUpPositionX = CalculateBubblePopUpXPosition(bubble.Index);
                bubble.PopUpPositionY = CalculateBubblePopUpYPosition((float)cl, bubble.Radius);
                bubble.Text = ((int)(100 * cl)).ToString(CultureInfo.InvariantCulture);
            }

            //To be uncomment for real data
            //var nowData = data.SingleOrDefault(d => d.DateTime.Date == dateTime.Date);

            //var selectedActivities = from bubble in _bubbles
            //                         from activity in nowData.Activities
            //                         where
            //                             bubble.ActivityName == activity.ActivityName &&
            //                             Math.Abs((activity.DateTime - dateTime).TotalMinutes) <= 60
            //                         select new { Activity = activity, Bubble = bubble };

            //foreach (var bubbleActivity in selectedActivities)
            //{
            //    bubbleActivity.Bubble.PopUpPositionX = CalculateBubblePopUpXPosition(bubbleActivity.Bubble.Index);
            //    bubbleActivity.Bubble.PopUpPositionY = CalculateBubblePopUpYPosition((float)bubbleActivity.Activity.ComfortLevel, bubbleActivity.Bubble.Radius);
            //    bubbleActivity.Bubble.Text = ((int)(100 * bubbleActivity.Activity.ComfortLevel)).ToString(CultureInfo.InvariantCulture);
            //}
        }

        public void ClearBubbles()
        {
            _bubbles.Clear();
        }

        public BasePhysicalSprite GetBubbleContainingPoint(CCPoint point)
        {
            return _bubbles.FirstOrDefault(bubble => bubble.BoundingBox.ContainsPoint(point));
        }

        public void MoveBubble(BasePhysicalSprite bubble, CCPoint location)
        {
            if (bubble == null)
            {
                return;
            }

            bubble.ChangePosition(location.X, location.Y);
        }

        public void ReleaseBubble(ref BasePhysicalSprite bubble)
        {
            if (bubble == null)
            {
                return;
            }

            bubble.RestoreDensity();

            bubble.IsTouched = false;

            bubble.ElasticReturnToPopUpPosition();

            if (bubble.CollidesWith.Count > 0)
            {
                bubble.CollidesWith.First().ElasticReturnToPopUpPosition();
            }
            bubble.CollidesWith.Clear();

            bubble = null;
        }
    }
}
