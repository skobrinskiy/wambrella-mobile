﻿using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace wambrella.Shared.WambrellaPlatform
{
    public class HttpRestClient
    {
        public async Task<string> MakeGetRequest(string query)
        {
            var request = (HttpWebRequest) WebRequest.Create(query);

            request.Method = "GET";

            var task = Task.Factory.FromAsync((cb, o) => ((HttpWebRequest) o).BeginGetResponse(cb, o),
                res => ((HttpWebRequest) res.AsyncState).EndGetResponse(res), request);
            using (var result = await task)
            using (var stream = result.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}