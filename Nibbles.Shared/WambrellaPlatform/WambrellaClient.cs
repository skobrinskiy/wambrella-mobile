﻿using System.Globalization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using wambrella.Shared.Model;

namespace wambrella.Shared.WambrellaPlatform
{
    public class WambrellaClient
    {
        public const string BaseAddress = "http://www.wambrella.com/WambrellaService/";
        public const string Rest = "rest/";

        public const string WambrellaServiceBaseAddress = BaseAddress + "Wambrella.svc/" + Rest;

        public const string WambrellaSkipassServiceBaseAddress = BaseAddress + "WambrellaSkiPass.svc/" + Rest;

        public const string WambrellaBusinessServiceBaseAddress = BaseAddress + "WambrellaBusiness.svc/" + Rest;

        public static Task<ObservationData> GetActivityComfortLevelsForCurrentLocation(double latitude,
            double longitude)
        {
            return GetActivityComfortLevelsForCurrentLocation(latitude, longitude, "actioncomfortlevelsforlocation");
        }

        public static Task<ObservationData> GetActivityComfortLevelsForCurrentLocationPastAndFutureByHours(double latitude,
            double longitude)
        {
            return GetActivityComfortLevelsForCurrentLocation(latitude, longitude, "actioncomfortlevelsforlocationpastfuturebyhours");
        }

        private static async Task<ObservationData> GetActivityComfortLevelsForCurrentLocation(double latitude, double longitude, string request)
        {
            var usLatitude = latitude.ToString(CultureInfo.InvariantCulture).Replace(",", ".");
            var usLongitude = longitude.ToString(CultureInfo.InvariantCulture).Replace(",", ".");

            var query = string.Format("{0}{1}?latitude={2}&longitude={3}", WambrellaServiceBaseAddress, request, usLatitude, usLongitude);

            var client  = new HttpRestClient();
            var res = await client.MakeGetRequest(query);
            return JsonConvert.DeserializeObject<ObservationData>(res);
        }
    }
}
